import React from "react";
import { Component } from "react/cjs/react.production.min";
import "./App.css";
import Counters from "./components/counters";
import NavBar from "./components/navbar";

class App extends Component {
  state = {
    counters: [
      { id: 1, value: 5 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 1 },
    ],
  };

  handleIncrement = (counter) => {
    // clone the counters array to break reference to we can´t modify the state directly
    const counters = [...this.state.counters];
    // get the counter id
    const index = counters.indexOf(counter);
    // depend of id, clone the counter element to match
    counters[index] = { ...counter };
    // increment value of counter
    counters[index].value++;
    // update the state
    this.setState({ counters });
  };

  handleDecrement = (counter) => {
    const counters = [...this.state.counters];
    // get the counter id
    const index = counters.indexOf(counter);
    // depend of id, clone the counter element to match
    counters[index] = { ...counter };
    // increment value of counter
    counters[index].value--;
    // update the state
    this.setState({ counters });
  };

  handleDelete = (counterId) => {
    const counters = this.state.counters.filter((c) => c.id !== counterId);
    this.setState({ counters }); // it's the same --> {counters: counters}
  };

  handleReset = () => {
    // all counters reset to zero
    const counters = this.state.counters.map((c) => {
      c.value = 0;
      return c; ///aqui me quedo duda
    });
    this.setState({ counters });
  };

  render() {
    return (
      <React.Fragment>
        <NavBar
          totalCounters={this.state.counters.filter((c) => c.value > 0).length}
        />
        <main className="container">
          <Counters
            counters={this.state.counters}
            onIncrement={this.handleIncrement}
            onDecrement={this.handleDecrement}
            onDelete={this.handleDelete}
            onReset={this.handleReset}
          />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
