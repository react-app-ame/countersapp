import React, { Component } from "react";
class Counter extends Component {
  render() {
    console.log(this.props);
    return (
      <React.Fragment>
        <div className="col-1">
          <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
        </div>
        <div className="col-1">
          <button
            onClick={() => this.props.onIncrement(this.props.counter)}
            className="btn btn-outline-secondary btn-sm"
          >
            <i className="bi bi-plus-lg"></i>
          </button>
        </div>
        <div className="col-1">
          <button
            onClick={() => this.props.onDecrement(this.props.counter)}
            className="btn btn-outline-secondary btn-sm"
            disabled={this.props.counter.value === 0 ? "disable" : ""}
          >
            <i className="bi bi-dash-lg"></i>
          </button>
        </div>
        <div className="col-1">
          <button
            onClick={() => this.props.onDelete(this.props.counter.id)}
            className="btn btn-outline-danger btn-sm"
          >
            <i className="bi bi-trash3-fill"></i>
          </button>
        </div>
      </React.Fragment>
    );
  }

  getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += this.props.counter.value === 0 ? "warning" : "primary";
    return classes;
  }

  formatCount() {
    const { value } = this.props.counter;
    return value === 0 ? "Zero" : value;
  }
}

export default Counter;
